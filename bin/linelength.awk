#!/usr/bin/awk -f
#
# linelength - count line length frequencies
# (excluding the 1st field which is timestamp added by Splunk)
#
BEGIN { print "timestamp,len,freq"; }

# Read throught the input and keep track of
# the lastest timestamp for each word/field
{ l = length($0) - length($1) - 1; ll[l]++; ts = $1 }

END { for (len in ll) printf "%s,%s,%s\n", ts, len, ll[len] }
