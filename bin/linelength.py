#!/usr/bin/python
import sys, os
import csv
import subprocess
import splunk.Intersplunk
import StringIO

my_prog = os.path.join(sys.path[0],'linelength.awk')

reader = csv.DictReader(sys.stdin)
p = subprocess.Popen(my_prog, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

# parse _time and _raw -fields from Splunk and pass them to your script stdin

for row in reader:
   p.stdin.write(row['_time'] + " " + row['_raw'] + '\n')

# wait for the program to terminate and print it's output back to Splunk
# NOTE: your script must produce CSV output with column headers on first row!

results = []
output = p.communicate()[0]

f = StringIO.StringIO(output)
reader = csv.DictReader(f)

# this is assuming event timestamp is retained 
# in "timestamp" -column of script output

for row in reader:
   splunk_time = row['timestamp']
   # delete fields that are not wanted in key=value output for Splunk
   del row['timestamp']
   # generate _raw field of key=value -pairs for Splunk to parse
   # Splunk can automatically recognize and parse key=value -format
   splunk_raw = []
   for key in row:
      splunk_raw.append(key + '=' + row[key])   
   row['_raw'] = ','.join(splunk_raw)
   row['_time'] = splunk_time
   
   results.append(row)
   
splunk.Intersplunk.outputResults(results)   

